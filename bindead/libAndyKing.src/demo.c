#include <iostream>
#include <string.h>

#include "simp/SimpSolver.h"
#include "core/SolverTypes.h"
#include "mtl/Vec.h"
#include "iface.h"

using namespace Minisat;

void printClause(int* clause);
bool project(vec<int*>& clauses, int scope, vec<int*>& projects);
bool double_implicants(vec<int*>& clauses, int scope, bool exists, vec<int*>& projects);

int main(int argc, char *argv[]) {
  void* clauses= makecnf();
  int clause0[] = { 2, 0+2*2, 1+2*0 }; // -x2 v x0
  int clause1[] = { 2, 0+2*1, 1+2*2 }; // -x1 v x2
  int clause2[] = { 1, 1+2*3 }; // x3
  int clause3[] = { 1, 0+2*3 }; // x3
  addclause(clauses, clause0);
  addclause(clauses, clause1);
  addclause(clauses, clause2);
  //clauses.push(clause3);
  int scope = 2;
  showcnf(clauses);
  
  void* projects=makecnf();
  invert(clauses, scope, projects);

  printf("----\n");
  showcnf(projects);

  void* projects2=makecnf();
  invert(projects, scope, projects2);
  
  printf("----\n");
  showcnf(projects2);
}
