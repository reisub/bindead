#include <iostream>
#include <string.h>
#include "simp/SimpSolver.h"
#include "core/SolverTypes.h"
#include "mtl/Vec.h"

using namespace Minisat;
using namespace std;

void printClause(int* clause);
void printImplicant(int* clause);

void bitonic(vec<Lit>& a, Solver& s);
void bitonic_sort(int lo, int n, bool dir, vec<Lit>& a, Solver& s);
void bitonic_merge(int lo, int n, bool dir, vec<Lit>& a, Solver& s);
void double_implicants(vec<int*>& clauses, int scope, bool exists, vec<int*>& primes);
bool project(vec<int*>& clauses, int scope, vec<int*>& projects);
void dualise(vec<int*>& implicants, int scope, vec<int*>& clauses);



bool projectInverse(vec<int*>& clauses, int scope, vec<int*>& projects)
{
	double_implicants(clauses, scope, true, projects);
	return projects.size()!=0;
}

bool project(vec<int*>& clauses, int scope, vec<int*>& projects)
{
	vec<int*> primes;
	primes.clear();
	double_implicants(clauses, scope, true, primes);

	if (primes.size() == 0) return false;
	else
	{
		dualise(primes, scope, projects);
		return projects.size();
	}
}

void dualise(vec<int*>& conjuncts, int scope, vec<int*>& clauses)
{	
	vec<int*> duals;
	for (int i = 0; i < conjuncts.size(); i++)
	{
		int* conjunct = conjuncts[i];
		int* dual = new int[conjunct[0] + 1];
		dual[0] = conjunct[0];
		for (int j = 0; j < conjunct[0]; j++)
		{
			dual[j + 1] = conjunct[j + 1] ^ 1;
		}
		duals.push(dual);
	}

	vec<int*> negatives;
	negatives.clear();
	double_implicants(duals, scope, false, negatives);

	for (int i = 0; i < negatives.size(); i++)
	{
		int* negative = negatives[i];
		int* clause = new int[negative[0] + 1];
		clause[0] = negative[0];
		for (int j = 0; j < negative[0]; j++)
		{
			clause[j+1] = negative[j+1] ^ 1;	
		}
		clauses.push(clause);
	}
}

void double_implicants(vec<int*>& clauses, int scope, bool exists, vec<int*>& primes)
{
	vec<Lit> pos_vars, neg_vars, occur_vars, vars;
	vec<Lit> pos_vars_prime, neg_vars_prime, occur_vars_prime, occur_vars_reverse, vars_prime;

	Solver s, t;

	for (int i = 0; i < scope; i++)
	{
		occur_vars_reverse.push(mkLit(t.newVar(false, true)));
	}

	for (int i = 0; i < scope; i++)
	{
		pos_vars.push(mkLit(s.newVar()));
		neg_vars.push(mkLit(s.newVar()));
		s.addClause(~pos_vars[i], ~neg_vars[i]);
		pos_vars_prime.push(mkLit(t.newVar()));
		neg_vars_prime.push(mkLit(t.newVar()));
		t.addClause(~pos_vars_prime[i], ~neg_vars_prime[i]);
	}
		
	for (int i = 0; i < scope; i++)
	{
		occur_vars.push(mkLit(s.newVar()));
		s.addClause(~occur_vars[i], pos_vars[i], neg_vars[i]);
		s.addClause(occur_vars[i], ~pos_vars[i]);
		s.addClause(occur_vars[i], ~neg_vars[i]);
		occur_vars_prime.push(mkLit(t.newVar(false, true)));
		t.addClause(~occur_vars_prime[i], pos_vars_prime[i], neg_vars_prime[i]);
		t.addClause(occur_vars_prime[i], ~pos_vars_prime[i]);
		t.addClause(occur_vars_prime[i], ~neg_vars_prime[i]);
	}

	int above = 1;
	while (above < scope)
	{
		above = above << 1;
	}

	for (int i = scope; i < above; i++)
	{
		occur_vars.push(mkLit(s.newVar()));
		s.addClause(~occur_vars[i]);
		occur_vars_prime.push(lit_Undef);
	}

	bitonic(occur_vars, s);
	bitonic(occur_vars_prime, t);
	
	for (int i = 0; i < scope; i++)
	{
		t.addClause(~occur_vars_prime[i],  occur_vars_reverse[scope - i - 1]);
		t.addClause( occur_vars_prime[i], ~occur_vars_reverse[scope - i - 1]);
	}

	for (int i = 0; i < clauses.size(); i++)
	{
		int* clause = clauses[i];
		vec<Lit> s_clause, t_clause;
		for (int j = 0; j < clause[0]; j++)
		{
			int positive = clause[j+1] & 1;
			int index = clause[j+1] >> 1;
			if (index < scope)
			{
				if (positive) s_clause.push(pos_vars[index]);
				else s_clause.push(neg_vars[index]);	

				if (positive) t_clause.push(pos_vars_prime[index]);
				else t_clause.push(neg_vars_prime[index]);	
			}
			else if (exists)
			{
				while (index >= vars.size())
				{
					vars.push(mkLit(s.newVar()));
				}
				if (positive) s_clause.push(vars[index]);
				else s_clause.push(~vars[index]);	

				while (index >= vars_prime.size())
				{
					vars_prime.push(mkLit(t.newVar()));
				}
				if (positive) t_clause.push(vars_prime[index]);
				else t_clause.push(~vars_prime[index]);	
			}
		}
		s.addClause(s_clause);
		t.addClause(t_clause);
		s_clause.clear();
		t_clause.clear();
	}
	s.simplify();
	t.simplify();

	int shortest = -1;
	for (int i = 1; i <= scope; i++)
	{
		vec<Lit> s_assume;
		for (int j = 0; j < i; j++)
		{
			s_assume.push(occur_vars[j]);
		}
		for (int j = i; j < scope; j++)
		{
			s_assume.push(~occur_vars[j]);
		}

		while (s.solve(s_assume))
		{
			if (shortest < 0) shortest = i;
			
			vec<int> implicant_in;
			vec<int> implicant_out;
			implicant_in.clear();
			implicant_out.clear();

			for (int k = 0; k < scope; k++)
			{
				if (s.modelValue(2 * k) == l_True) implicant_in.push((k << 1) + 1);	
				else implicant_out.push((k << 1) + 1);
				if (s.modelValue(2 * k + 1) == l_True) implicant_in.push(k << 1);	
				else implicant_out.push(k << 1);
			}

			if (implicant_in.size() > shortest) 
			{
				vec<Lit> t_assume;
				t_assume.clear();

				int strides[5] = {0, 1, 2, 4, 8};
				int stride = 2; 
					
				while (strides[stride] >= implicant_in.size())
				{
					stride--;
				}

				while (strides[stride] > 0)
				{
					for (int k = 0; k < implicant_out.size(); k++)
					{
						if (implicant_out[k] & 1) t_assume.push(~pos_vars_prime[implicant_out[k] >> 1]);	
						else t_assume.push(~neg_vars_prime[implicant_out[k] >> 1]);	
					}
		
					t_assume.push(~occur_vars_reverse[scope - implicant_in.size() + strides[stride] - 1]); 
				
					if (t.solve(t_assume))
					{
						implicant_in.clear();
						implicant_out.clear();

						for (int k = 0; k < scope; k++)
						{
							if (t.modelValue(scope + 2 * k) == l_True) implicant_in.push((k << 1) + 1);	
							else implicant_out.push((k << 1) + 1);

							if (t.modelValue(scope + 2 * k + 1) == l_True) implicant_in.push(k << 1);	
							else implicant_out.push(k << 1);
						}

						while (strides[stride] >= implicant_in.size())
						{
							stride--;
						}

					}
					else
					{
						stride--;
					}
				}
			}

			vec<Lit> block;
			for (int k = 0; k < implicant_in.size(); k++)
			{
				if (implicant_in[k] & 1) block.push(neg_vars[implicant_in[k] >> 1]);
				else block.push(pos_vars[implicant_in[k] >> 1]);
			}

			int *my_implicant = new int[implicant_in.size() + 1];
			my_implicant[0] = implicant_in.size();
			for (int k = 0; k < implicant_in.size(); k++)
			{
				my_implicant[k+1] = implicant_in[k];
			}
			primes.push(my_implicant);

			s.addClause(block);
			s.simplify();
			block.clear();

		}
		s_assume.clear();

	}
}

void printLiterals(int* clause, char connect)
{
	printf("(");
	for (int i = 0; i < clause[0]; i++)
        {
	    int lit = clause[i+1];
            if (lit & 1) printf("%d", lit >> 1);
            else printf("-%d", lit >> 1);
	    if (i + 1 < clause[0]) printf(" %c ", connect);
        }
        printf(")\n");
}

void printClause(int* clause)
{
	printLiterals(clause, 'v');
}

void printImplicant(int* clause)
{
	printLiterals(clause, '^');
}

void bitonic(vec<Lit>& a, Solver& s)
{
  	bitonic_sort(0, a.size(), false, a, s);
}
    
void bitonic_sort(int lo, int n, bool dir, vec<Lit>& a, Solver& s)
{
	if (n > 1)
        {
            int m=n/2;
            bitonic_sort(lo, m, true, a, s);
            bitonic_sort(lo+m, m, false, a, s);
            bitonic_merge(lo, n, dir, a, s);
        }
}

void bitonic_merge(int lo, int n, bool dir, vec<Lit>& a, Solver& s)
{
        if (n > 1)
        {
            int m = n/2;
            for (int i=lo; i < lo+m; i++)
	    {
		Lit x = a[i];
		Lit y = a[i+m];
		Lit x_prime, y_prime;

		if (dir)
		{
			if (x == lit_Undef)
			{
				x_prime = x;
				y_prime = y;
			}
			else if (y == lit_Undef)
			{
				x_prime = y;
				y_prime = x;
			}
			else
			{
				x_prime = mkLit(s.newVar());
				y_prime = mkLit(s.newVar());

				// x_prime <=> x /\ y
				s.addClause(x_prime, ~x, ~y);
				s.addClause(~x_prime, x);
				s.addClause(~x_prime, y);
				// y_prime <=> x \/ y
				s.addClause(~y_prime, x, y);
				s.addClause(y_prime, ~x);
				s.addClause(y_prime, ~y);
			}
		}
		else
		{
			if (x == lit_Undef)
			{
				x_prime = y;
				y_prime = x;
			}
			else if (y == lit_Undef)
			{
				x_prime = x;
				y_prime = y;
			}
			else
			{
				x_prime = mkLit(s.newVar());
				y_prime = mkLit(s.newVar());
			
				// x_prime <=> x \/ y
				s.addClause(~x_prime, x, y);
				s.addClause(x_prime, ~x);
				s.addClause(x_prime, ~y);
				// y_prime <=> x /\ y
				s.addClause(y_prime, ~x, ~y);
				s.addClause(~y_prime, x);
				s.addClause(~y_prime, y);
			}
		}

		a[i] = x_prime;
		a[i+m] = y_prime;
	    }
            bitonic_merge(lo, m, dir, a, s);
            bitonic_merge(lo+m, m, dir, a, s);
        }
}
