#include "mtl/Vec.h"
#include "iface.h"
#include "Demo.h"

using namespace Minisat;

void printClause(int* clause);
void double_implicants(vec<int*>& clauses, int scope, bool exists, vec<int*>& primes);


void* makecnf() {
  vec<int*>* cnf = new vec<int*>();
  cnf->clear();
  return cnf;
}

int*  popclause(void* cnf) {
  if(((vec<int*>*) cnf)->size()<=0)
    return 0;
  int* c = ((vec<int*>*) cnf)->last();
  ((vec<int*>*) cnf)->pop();
  return c;
}

void destroycnf(void*cnf) {
  int* c;
  while((c=popclause(cnf)))
    free(c);
  delete (vec<int*>*) cnf;
}

void showcnf(void* cnf) {
  printf("cnf:\n");
  for (int i = 0; i < ((vec<int*>*)cnf)->size(); i++)
    printClause((*((vec<int*>*)cnf))[i]);
}

void addclause(void* cnf, int* clause) {
  ((vec<int*>*) cnf)->push(clause);
}

void invert(void* cnf, int scope, void* dnf) {
  ((vec<int*>*)dnf)->clear();
  double_implicants(*((vec<int*>*)cnf), scope, true, *((vec<int*>*)dnf));
}


/*
  Java stuff
*/


JNIEXPORT jlong JNICALL Java_satDomain_NativeInverter_makecnf(JNIEnv *, jobject) {
  return (jlong)makecnf();
}

JNIEXPORT void JNICALL Java_satDomain_NativeInverter_destroycnf(JNIEnv *, jobject, jlong cnf) {
  destroycnf((void*)cnf);
}

JNIEXPORT void JNICALL Java_satDomain_NativeInverter_showcnf(JNIEnv *, jobject, jlong cnf) {
  showcnf((void*)cnf);
}

JNIEXPORT void JNICALL Java_satDomain_NativeInverter_addclause(JNIEnv * env, jobject, jlong cnf, jintArray arr) {
  jint i, sum = 0;
  int* clause;
  int len = env->functions->GetArrayLength(env, arr);
  jint * carr = env->functions->GetIntArrayElements(env, arr, NULL);
  clause = (int*)malloc((len+1)* sizeof( int));
  if (carr == NULL) {
    printf("error accessing array :(");
    return;
  }
  clause[0]=len;
  for (i=0; i<len; i++) {
    int v = carr[i];
    clause[i+1] = v<0? -2*v : 2*v+1;
  }
  (env->functions)->ReleaseIntArrayElements(env, arr, carr, 0);
  addclause((void*)cnf,clause);  
}


JNIEXPORT jintArray JNICALL Java_satDomain_NativeInverter_popclause(JNIEnv * env, jobject, jlong cnf) {
  int * c = popclause((void*) cnf);
  if(c==0)
    return 0;
  jintArray result;
  int size=c[0];
  result = env->functions->NewIntArray(env, size);
  if (result == NULL) {
    return NULL;
  }
  env->functions->SetIntArrayRegion(env, result, 0, size, c+1);
  free(c);
  return result;
}


JNIEXPORT void JNICALL Java_satDomain_NativeInverter_invert(JNIEnv *, jobject, jlong cnf, jint scope, jlong dnf) {
  invert((void*)cnf, scope, (void*)dnf);
}
