
extern "C" void* makecnf(); 
extern "C" void destroycnf(void*cnf);
extern "C" void showcnf(void* cnf);
extern "C" void addclause(void* cnf, int* clause);
extern "C" void invert(void* cnf, int scope, void* dnf);
