package bindead.environment.platform;

import java.util.HashMap;
import java.util.Map;

import rreil.gdsl.builder.IdBuilder;
import rreil.lang.MemVar;
import bindead.analyses.Bootstrap;
import bindead.domainnetwork.interfaces.RegionCtx;
import bindead.domainnetwork.interfaces.RootDomain;
import binspot.gdsl.GdslX86_32Disassembler;

/**
 * Platform for Intel x86-32bit processors using the old disassembler implemented in Java.
 */
public class GdslX86_32Platform extends Platform {
  private static Platform INSTANCE;
  private static final String stackPointerName = "esp";
  private static final String instructionPointerName = "eip";

  private GdslX86_32Platform () {
    super(GdslX86_32Disassembler.instance);
    Map<String, String> renamings = new HashMap<String, String>() {{
      put("virt_leu", "BE");
      put("virt_lts", "LT");
      put("virt_les", "LE");
      put("a", "eax");
      put("b", "ebx");
      put("c", "ecx");
      put("d", "edx");
      put("si", "esi");
      put("di", "edi");
      put("bp", "ebp");
      put("sp", "esp");
      put("ip", "eip");
    }};
    IdBuilder.setRenamings(renamings);
  }

  public static Platform instance () {
    if (INSTANCE == null)
      INSTANCE = new GdslX86_32Platform();
    return INSTANCE;
  }

  /**
   * {@inheritDoc}
   */
  @Override public Bootstrap forwardAnalysisBootstrap () {
    return new Bootstrap() {
      @Override public <D extends RootDomain<D>> D bootstrap (D state, long initialPC) {
        // TODO: only here for the old ROOT domain. remove when root is deleted
        state = state.introduceRegion(MemVar.getVarOrFresh("stack"), RegionCtx.EMPTYSTICKY);
        state = initializeStackPointer(state);
        // set initial value of GDSL stack-segment base register
        state = initializeSegmentRegisters(state);
        state = initializeInstructionPointer(state, initialPC);
        return state;
      }
    };
  }

  static <D extends RootDomain<D>> D initializeSegmentRegisters (D state) {
    String[] segPrefixes = {"e", "d", "s", "f", "g", "c"};
    for (String segPrefix : segPrefixes) {
      // FIXME: the segments registers are currently 64 bits on all x86 platforms.
      // Retrieve it from the platform config.
      state = state.eval(String.format("mov.%d %s, 0", 64, segPrefix + "s_base"));
    }
    return state;
  }

  @Override public String getStackPointer () {
    return stackPointerName;
  }

  @Override public String getInstructionPointer () {
    return instructionPointerName;
  }

  @Deprecated @Override public MemVar getStackRegion () {
    return MemVar.getVarOrFresh("stack.x86_32");
  }

}
