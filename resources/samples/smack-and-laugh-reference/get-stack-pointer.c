

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

uint64_t getStackPointer64 () {
  __asm__("movq %rsp, %rax");
}

void main () {
  uint64_t sp = getStackPointer64();
  printf("SP: %08x\n", sp);
}
