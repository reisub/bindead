#include "../include/reach.h"

// In this testcase, COUNT is defined to be 5-9, as indicated by the results in
// the table.

int main()
{
  int n1,n2, z;
  int m=0;
  while(n1>=0) // n1 is the input
    {
      m++;
      n1--;
    }
  while(n2>=0) // n2 is the input
    {
      m++;
      n2--;
    }
  if(m==COUNT) // not input dependent
    REACHABLE();
  else
    z=9;
  return 0;
}



