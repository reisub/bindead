void bar() {
  // address here is 0x10
  REACHABLE();
  return; // return to main
}

void foo() {
  int flip = MakeChoice() & 1;
  int r =  ( flip * 0x68 ) + // return address in main for call on foo
           ( ( 1 - flip ) * 0x10); // beginning of bar
  *(&r+2) = r; // clobber return address
  return;
}

int main() {
  foo();
  // address here is 0x68
  return 0;
}
