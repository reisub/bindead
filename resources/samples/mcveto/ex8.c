#include "../include/reach.h"

#define HEAP_SIZE (1<<10)
char my_heap_data[HEAP_SIZE]={0};
unsigned int my_heap_index=0;

char* mymalloc(unsigned int n) 
{
  char *p;
  if(n==0)
    return 0;
  p = &my_heap_data[my_heap_index];
  if(my_heap_index+n >= HEAP_SIZE)
    return 0;
  my_heap_index+=n;
  return p;
}

int MakeChoice() {return 0;}

int main(){

   int * a;
   int k = MakeChoice();
   int i;
   if ( k <= 0 ) return -1;
   
   a= (int *)mymalloc( k * sizeof(int));
   if(!a) return 1;//
   
   for (i =0 ; i != k; i++)
     {
       if(i>=k) UNREACHABLE();
       if (a[i]) return 1;
     }

   return 0;

}
