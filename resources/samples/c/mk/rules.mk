
SRCDIR	= src
BINDIR  = build/$objtype

SRC	=\
  array-unsigned-int.c\
  array-int.c\
  array-short.c\
  duffcopy.c\
  even-odd.c\
  function-pointer.c\
  output-argument.c\
  switch1.c\
  switch2.c\
  switch3.c\
  union.c

T	= ${SRC:%.c=$BINDIR/%}

all:V:	$BINDIR $T

$BINDIR:
	mkdir -p $BINDIR

$BINDIR/%: $SRCDIR/%.c
	$CC $CFLAGS -o $target $prereq $LDFLAGS	
			
clean:V:
	rm $T
