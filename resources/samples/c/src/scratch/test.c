
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "test.h"

int sll (int x)
{
   return (x << 6);
}

/*
uint64_t read2 ()
{
   __asm__ ("movq %fs:0x28,%rax");
}   

uint64_t read () {
   return (*((uint64_t *)(NULL + 0x28)));
}
*/
int main ( int argc, char** argv)
{
    int n;
    unsigned long rsp;


    //printf ("%llu", (unsigned long long)read2 ());
    //printf ("%llu", (unsigned long long)read ());
    //rsp = getRSP ();

    n = testStr0 ();

    //testThreads ( 1);

    n = argc;

    n = testSwitch1 (n, n);
    printf ("%d\n", n);

    n = testSwitch1 (5, 1);
    printf ("%d\n", n);

    n = testEffectFn ();
    printf ("%d\n", n);

    printf ("RSP: %lx\n", rsp);

    return (EXIT_SUCCESS);
}
