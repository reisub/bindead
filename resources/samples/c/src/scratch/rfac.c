
#include "test.h"

int rfac ( int) __attribute__ ((noinline));
int rfac ( int n)
{
    if (n <= 1)
        return (1);
    else
        return (n * rfac (n - 1));
}

int runRFac ()
{
    int r, n = 3;

    r = rfac ( n);

    return (r);
}
