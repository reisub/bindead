
/* Public Domain. All warranties are disclaimed. */
 
#include "test.h"

#define False   0
#define True    1

int even ( int    n)
{
    if (n == 0)
	return (True);
    else
	return (odd (n - 1));
}

int odd ( int    n)
{
    if (n == 0)
	return (False);
    else
	return (even (n - 1));
}
