
#include <stdlib.h>
#include <stdio.h>

#include "test.h"

typedef int (*fn)(int, int);

int f1 (int a, int b);
int f2 (int a, int b);
int f3 (int a, int b);
int f4 (int a, int b);

fn a[4] = { f1, f2, f3, f4};

int f1 ( int a, int b)
{
    return (a + b);
}

int f2 ( int a, int b)
{
    return (a + b);
}

int f3 ( int a, int b)
{
    return (a + b);
}

int f4 ( int a, int b)
{
    return (a + b);
}

void testFPtr ( int i)
{
    int r;

    if (i < 0 || i > 4)
        return;

    r = i + a[i](i, 1);

    printf ("i: %d", i);
}

int apply ( fn, int, int) __attribute__((noinline));
int apply ( fn fn, int a, int b)
{
    return (fn (a, b));
}

int testFPtrTailCall (int i)
{
    return (apply (&f1, i, 1));
}