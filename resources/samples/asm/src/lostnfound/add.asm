
format elf64
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   add ah, bl
   add al, bl
   add ax, bx
   add eax, ebx

   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
