
format elf
	
section '.data' writable

foo		dw 0x2342
msg		db 'Hello World64!', 0x0A
msg_size	= $-msg

section '.text' executable

public main

main:
   add ah, bl
   add al, bl
   add ax, bx
   add eax, ebx
   add bx, word [bx]
   add al, byte [eax+eax*4+0x23]
   add byte [eax+eax*4+0x23], al
   movhps xmm0, qword [eax]
   movhps qword [eax], xmm0
   ; exit
   xor     edi, edi
   mov     eax, 60
   syscall
