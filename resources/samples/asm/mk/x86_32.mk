
SRCDIR   = src/x32
SRC      = add.asm

CC	= gcc
CFLAGS	= -O2 -m32 -Wall
LDFLAGS = -O2 -m32
ASM	= fasm
ASMFLAGS = -felf32

O	= o
BIN	= build/$objtype
