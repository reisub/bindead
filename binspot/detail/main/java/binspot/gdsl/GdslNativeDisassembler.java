package binspot.gdsl;

import gdsl.Frontend;
import gdsl.Gdsl;
import gdsl.arch.AVRBinder;
import gdsl.arch.ArchBinder;
import gdsl.arch.ArchId;
import gdsl.arch.IConfigFlag;
import gdsl.arch.X86Binder;
import gdsl.decoder.Decoder;
import gdsl.decoder.NativeInstruction;
import gdsl.translator.OptimizationOptions;
import gdsl.translator.TranslatedBlock;
import gdsl.translator.Translator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javalx.exceptions.UncheckedExceptionWrapper;
import rreil.gdsl.BindeadGdslRReilBuilder;
import rreil.lang.RReilAddr;
import binspot.DecodeException;
import binspot.DecodeStream;
import binspot.NativeDisassembler;

public class GdslNativeDisassembler implements NativeDisassembler {
  private final Frontend frontend;

  public GdslNativeDisassembler (ArchId arch, IConfigFlag[] flags) {
    ArchBinder binder;
    switch (arch) {
    case X86:
      binder = new X86Binder();
      break;
    case AVR:
      binder = new AVRBinder();
      break;
    default:
      throw new IllegalArgumentException();
    }

    binder.resetConfig();
    for (IConfigFlag iConfigFlag : flags)
      binder.setConfigFlag(iConfigFlag);

    this.frontend = binder.getFrontend();
  }

  private Gdsl initGdsl (DecodeStream in, long pc) {
    Gdsl gdsl = new Gdsl(frontend);
    gdsl.setCode(in.getBuffer(), in.getIdx(), pc);
    return gdsl;
  }

  @Override public GdslNativeInstruction decode (DecodeStream in, long pc)
      throws DecodeException {
    Gdsl gdsl = initGdsl(in, pc);

    Decoder decoder = new Decoder(gdsl);
    NativeInstruction insn = decoder.decodeOne();

    int insnSize = (int) insn.getSize();
    byte[] code = new byte[insnSize];
    for (int i = 0; i < code.length; i++)
      code[i] = (byte) in.read8();

    return new GdslNativeInstruction(insn, code, pc);
  }

  @Override public GdslBlockOfInstructions decodeBlock (DecodeStream in, long pc) {
    Gdsl _gdsl = initGdsl(in, pc);

    Translator translator = new Translator(_gdsl, new BindeadGdslRReilBuilder(RReilAddr.valueOf(pc)));
    // FIXME: set optimizations from analysisproperties by oring the flags below
    // Note that preserve context/block only make sense when used together with blockwise disassembly!
    TranslatedBlock gBlock =
      translator.translateOptimizeBlock(Integer.MAX_VALUE,
          OptimizationOptions.PRESERVE_CONTEXT.and(OptimizationOptions.LIVENESS).and(OptimizationOptions.FSUBST));

    ByteArrayOutputStream bo = new ByteArrayOutputStream();
    NativeInstruction[] gInsns = gBlock.getInstructions();
    for (int i = 0; i < gInsns.length; i++) {
      NativeInstruction gInsn = gInsns[i];
      try {
        bo.write(in.read((int) gInsn.getSize()));
      } catch (IOException e) {
        e.printStackTrace();
        throw new UncheckedExceptionWrapper(e);
      }
    }

    return new GdslBlockOfInstructions(gBlock, bo.toByteArray(), pc);
  }
}
